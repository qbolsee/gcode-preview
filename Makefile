ifeq ($(OS),Windows_NT)
    LINK_DIR = lib_windows
    LIBS = -lglew32 -lglfw3 -lopengl32 -lgdi32
    OUTPUT_PATH = Release/Windows
else
    UNAME_S := $(shell uname -s)
    ifeq ($(UNAME_S),Linux)
        LINK_DIR = lib_linux
        LIBS = -lGLEW -lGLU -lGL -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi
        OUTPUT_PATH = Release/Linux
    else
        $(error Unknwonw OS, can't link libraries)
    endif
endif

LINK_PATH = -Llibs/glew-1.11.0/$(LINK_DIR) -Llibs/glfw-3.0.4/$(LINK_DIR)
CC = g++
CCFLAGS = -Wall
INCLUDE_PATH = -Ilibs/glew-1.11.0/include -Ilibs/glfw-3.0.4/include -Ilibs/glm

all: gcode_preview

gcode_preview: src/Opengl-render.cpp src/parser.cpp src/shader.cpp
	$(CC) $(CCFLAGS) $(INCLUDE_PATH) $(LINK_PATH) -o $(OUTPUT_PATH)/$@ $^ $(LIBS)

clean:
	rm -f *.o *.a
