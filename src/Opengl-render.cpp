#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include "shader.hpp"
#include "parser.h"
#define BACKGROUND_R (0.98f)
#define BACKGROUND_G (0.98f)
#define BACKGROUND_B (0.98f)
#define RECT_R (0.f)
#define RECT_G (0.4f)
#define RECT_B (0.8f)

#ifdef __unix__
#include <unistd.h>
#elif defined(_WIN32) || defined(WIN32)
#include <Windows.h>
#define OS_Windows
#endif

using namespace glm;

const char* vertex_shader = "#version 330 core\n"
                            "in vec3 v_coord;\n"
                            "in vec3 v_color;\n"
                            "in float v_alpha;\n"
                            "uniform mat4 my_mat;\n"
                            "out vec3 f_color;\n"
                            "flat out float f_alpha;\n"
                            "\n"
                            "void main() {\n"
                            "	vec4 v = vec4(v_coord, 1.0);\n"
                            "    gl_Position = my_mat*v;\n"
                            "    f_color = v_color;\n"
                            "    f_alpha = v_alpha;\n"
                            "}\n";

const char* fragment_shader = "#version 330 core\n"
                              "in vec3 f_color;\n"
                              "flat in float f_alpha;\n"
                              "layout(location = 0) out vec3 color;\n"
                              "\n"
                              "void main() {\n"
                              "    if (f_alpha == 0.0) {\n"
                              "        discard;\n"
                              "    }\n"
                              "    color = f_color;\n"
                              "}\n";

static int window_width = 1024;
static int window_height = 768;

static float rect_width = 297.f;
static float rect_height = 210.f;

static float PIXEL_PER_MM = 3.1f;

static float view_scale_target = 1.f;
static float view_scale = 1.f;
static vec3 mouse_pos(0.f, 0.f, 0.f);
static vec3 view_offset_target(0.f, 0.f, 0.f);
static vec3 view_offset(0.f, 0.f, 0.f);

void callback_window_resize(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
    window_width = width;
    window_height = height;
}

void callback_keyboard(GLFWwindow* window, int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS || action == GLFW_REPEAT) {
        switch (key) {
            case GLFW_KEY_KP_ADD:
                view_scale_target *= 1.2f;
                break;
            case GLFW_KEY_KP_SUBTRACT:
                view_scale_target *= 0.8f;
                if (view_scale_target < 0.2f) {
                    view_scale_target = 0.2f;
                }
                break;
        }
    }
}

void callback_mouse_scroll(GLFWwindow* window, double xoffset, double yoffset) {
    view_scale_target *= (1 + 0.1f * yoffset);
    if (view_scale_target < 0.2f) {
        view_scale_target = 0.2f;
    }
}

void callback_mouse_move(GLFWwindow* window, double xpos, double ypos) {
    double xpos_mm;
    double ypos_mm;
    static bool not_initialized = true;
    switch (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT)) {
        case GLFW_PRESS:
            xpos_mm = xpos/(view_scale * PIXEL_PER_MM);
            ypos_mm = ypos/(view_scale * PIXEL_PER_MM);
            if (not_initialized) {
                mouse_pos.x = xpos_mm;
                mouse_pos.y = ypos_mm;
                not_initialized = false;
            } else {
                view_offset_target.x += xpos_mm - mouse_pos.x;
                view_offset_target.y += -(ypos_mm - mouse_pos.y);
                mouse_pos.x = xpos_mm;
                mouse_pos.y = ypos_mm;
            }
            break;
        case GLFW_RELEASE:
            not_initialized = true;
            break;
    }
}

void print_mat(mat4 &mat) {
    for (int i = 0; i < 16; i++) {
        printf("%7.4f ", mat[i % 4][i / 4]);
        if ((i+1)%4 == 0) {
            printf("\n");
        }
    }
}

void render_line(GLuint vbo_vertex, int n_vertex) {
    glBindBuffer(GL_ARRAY_BUFFER, vbo_vertex);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
        0,                            // attribute number
        3,                            // size
        GL_FLOAT,                     // type
        GL_FALSE,                     // normalized?
        VERTEX_PROPS*sizeof(GLfloat), // stride
        0                             // array buffer offset
    );

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
        1,
        4,
        GL_FLOAT,
        GL_FALSE,
        VERTEX_PROPS*sizeof(GLfloat),
        (void*)(3*sizeof(GLfloat))
    );

    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
        2,
        1,
        GL_FLOAT,
        GL_FALSE,
        VERTEX_PROPS*sizeof(GLfloat),
        (void*)(6*sizeof(GLfloat))
    );

    glDrawArrays(GL_LINE_STRIP, 0, n_vertex);

    glDisableVertexAttribArray(0);
    glDisableVertexAttribArray(1);
    glDisableVertexAttribArray(2);
}

void update_view(mat4 &mat_scale, mat4 &mat_translate, mat4 &mat) {
    view_scale += (view_scale_target - view_scale) * 0.1f;
    mat_scale[0][0] = (2*view_scale*PIXEL_PER_MM)/window_width;
    mat_scale[1][1] = (2*view_scale*PIXEL_PER_MM)/window_height;
    view_offset += (view_offset_target - view_offset) * 0.3f;
    mat_translate[3][0] = -rect_width/2 + view_offset.x;
    mat_translate[3][1] = -rect_height/2 + view_offset.y;
    mat = mat_scale * mat_translate;
}

int main(int argc, char* argv[]) {
    char working_directory[100];
    #ifdef OS_Windows
    GetCurrentDirectory(100, working_directory);
    #else
    readlink("/proc/self/exe", working_directory, 100);
    #endif

    if (argc == 1) {
        printf("usage: %s filename\n", argv[0]);
        return 0;
    }

    const char* filename = argv[1];

    int n_shape_vertex = get_file_length(filename);

    if (n_shape_vertex == 0) {
        printf("file not found\n");
        return -1;
    }

    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window; // (In the accompanying source code, this variable is global)
    window = glfwCreateWindow(window_width, window_height, "gcode preview", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
        glfwTerminate();
        return -1;
    }

    glfwSetWindowSizeCallback(window, callback_window_resize);
    glfwSetKeyCallback(window, callback_keyboard);
    glfwSetScrollCallback(window, callback_mouse_scroll);
    glfwSetCursorPosCallback(window, callback_mouse_move);

    glfwMakeContextCurrent(window); // Initialize GLEW
    glewExperimental = true; // Needed in core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }

    GLuint programID = compile_shaders(vertex_shader, fragment_shader);

    if (programID == 0) {
        glfwTerminate();
        return -1;
    }

    GLfloat rect_vertex[VERTEX_PROPS * 5] = {0.f, 0.f, 0.f, RECT_R, RECT_G, RECT_B, 1.f,
                                             rect_width, 0.f, 0.f, RECT_R, RECT_G, RECT_B, 1.f,
                                             rect_width, rect_height, 0.f, RECT_R, RECT_G, RECT_B, 1.f,
                                             0.f, rect_height, 0.f, RECT_R, RECT_G, RECT_B, 1.f,
                                             0.f, 0.f, 0.f, RECT_R, RECT_G, RECT_B, 1.f
    };

    GLfloat* shape_vertex = (GLfloat*)malloc(VERTEX_PROPS * n_shape_vertex * sizeof(GLfloat));
    parse_vertex(filename, shape_vertex);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    GLuint vbo_rect_vertex;
    glGenBuffers(1, &vbo_rect_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_rect_vertex);
    glBufferData(GL_ARRAY_BUFFER, VERTEX_PROPS * 5 * sizeof(GLfloat), rect_vertex, GL_STATIC_DRAW);

    GLuint vbo_shape_vertex;
    glGenBuffers(1, &vbo_shape_vertex);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_shape_vertex);
    glBufferData(GL_ARRAY_BUFFER, VERTEX_PROPS * n_shape_vertex * sizeof(GLfloat), shape_vertex, GL_STATIC_DRAW);

    glClearColor(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B, 0.0f);

    glm::mat4 mat_scale = glm::mat4(1.0f);

    glm::mat4 mat_translate = glm::mat4(1.0f);

    glm::mat4 mat = glm::mat4(1.0f);

    GLuint MatrixID = glGetUniformLocation(programID, "my_mat");

    do {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        glUseProgram(programID);

        update_view(mat_scale, mat_translate, mat);

        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &mat[0][0]);

        render_line(vbo_shape_vertex, n_shape_vertex);

        render_line(vbo_rect_vertex, 5);

        glfwSwapBuffers(window);
        glfwPollEvents();
    } while (glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);
    glDeleteBuffers(1, &vbo_shape_vertex);
    glDeleteVertexArrays(1, &VertexArrayID);

    glfwTerminate();

    free(shape_vertex);

    return 0;
}
