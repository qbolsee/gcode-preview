#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>

#ifndef SHADER_HPP
#define SHADER_HPP

GLuint compile_shaders(const char * vertex_file_path,const char * fragment_file_path);
GLuint load_shaders(const char * vertex_file_path,const char * fragment_file_path);

#endif
