#include <signal.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <GL/glew.h>
#define LINE_R (0.1f)
#define LINE_G (0.1f)
#define LINE_B (0.1f)
#define VERTEX_PROPS (7)
using namespace std;

const double nan_double = 100000.0;
const int nan_int = 100000;

struct t_gcode {
    double x;
    double y;
    int z;
    bool fast;
};

struct t_gcode_state {
    double x;
    double y;
    int z;
};

enum t_parse_state {
    e_null,
    e_parm,
};

int parse_line(string &line, t_gcode &instruction);

int get_file_length(const char* filename);

void parse_vertex(const char* filename, GLfloat* vertex);

void vertex_from_gcode(GLfloat* vertex_begin, t_gcode &instruction, t_gcode_state &state);
