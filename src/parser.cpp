#include "parser.h"
using namespace std;

void read_float(string &s, double &property) {
    char *endp;
    property = strtod(s.c_str() + 1, &endp);
    if (*endp != 0) {
        property = nan_double;
    }
}

void read_int(string &s, int &property) {
    char *endp;
    property = strtod(s.c_str() + 1, &endp);
    if (*endp != 0) {
        property = nan_int;
    }
}

int parse_line(string &line, t_gcode &instruction) {
    stringstream stream(line);
    string item;
    char first_char;
    t_parse_state state = e_null;

    while (getline(stream, item, ' ')) {
        switch (state) {
        case e_null:
            // read code
            if (!(item.compare("G0") || item.compare("G1"))) {
                return -1;
            }
            instruction.x = nan_double;
            instruction.y = nan_double;
            instruction.z = nan_int;
            instruction.fast = item.compare("G0") == 0;
            state = e_parm;
            break;
        case e_parm:
            first_char = item.c_str()[0];
            switch (first_char) {
                case 'X':
                    read_float(item, instruction.x);
                    break;
                case 'Y':
                    read_float(item, instruction.y);
                    break;
                case 'Z':
                    read_int(item, instruction.z);
                    break;
            }
            break;
        }
    }
    return 0;
}

int get_file_length(const char* filename) {
    ifstream file(filename);
    int count = 0;
    string line;
    if (file.is_open()) {
        while (getline(file, line)) {
            if (line.compare(0, 2, "G0") || line.compare(0, 2, "G1")) {
                count++;
            }
        }
        file.close();
    }
    return count;
}

void vertex_from_gcode(GLfloat* vertex_begin, t_gcode &instruction, t_gcode_state &state) {
    if (instruction.x != nan_double) {
        state.x = instruction.x;
    }
    if (instruction.y != nan_double) {
            state.y = instruction.y;
    }
    if (instruction.z != nan_int) {
            state.z = instruction.z;
    }
    *vertex_begin = state.x;
    *(vertex_begin + 1) = state.y;
    *(vertex_begin + 2) = 0;
    *(vertex_begin + 3) = LINE_R;
    *(vertex_begin + 4) = LINE_G;
    *(vertex_begin + 5) = LINE_B;
    *(vertex_begin + 6) = (state.z == 0) ? 1 : 0;// z = 1 means up (alpha = 0)
    /*
    printf("vertex: ");
    for (int i = 0; i < 7; i++) {
        printf("%6.2f ", *(vertex_begin + i));
    }
    printf("\n");
    */
}

void parse_vertex(const char* filename, GLfloat* vertex) {
    t_gcode instruction;
    t_gcode_state state;
    string line;
    ifstream file(filename);

    state.x = 0.0;
    state.y = 0.0;
    state.z = 1;

    while (getline(file, line)) {
        if (parse_line(line, instruction) == 0) {
            vertex_from_gcode(vertex, instruction, state);
            vertex += VERTEX_PROPS;
        }
    }

    file.close();
}
